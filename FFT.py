import sys
import numpy as np
import cv2
from matplotlib import pyplot as plt

# adapted from http://www.bogotobogo.com/python/OpenCV_Python/python_opencv3_Image_Fourier_Transform_FFT_DFT.php

img = cv2.imread(sys.argv[1], 0)

img_float32 = np.float32(img)

dft = cv2.dft(img_float32, flags=cv2.DFT_COMPLEX_OUTPUT)
dft_shift = np.fft.fftshift(dft)

magnitude_spectrum = 20 * np.log(cv2.magnitude(dft_shift[:, :, 0], dft_shift[:, :, 1]))
phase_spectrum = 20 * np.log(cv2.phase(dft_shift[:, :, 0], dft_shift[:, :, 1]))

reconstruction = cv2.idft(np.fft.ifftshift(dft_shift))
reconstruction_magnitude = cv2.magnitude(reconstruction[:,:,0],reconstruction[:,:,1])

plt.subplot(2, 2, 1), plt.imshow(img, cmap='gray')
plt.title('Input Image'), plt.xticks([]), plt.yticks([])
plt.subplot(3, 2, 2), plt.imshow(magnitude_spectrum, cmap='gray')
plt.title('Magnitude Spectrum'), plt.xticks([]), plt.yticks([])
plt.subplot(2, 2, 3), plt.imshow(phase_spectrum, cmap='gray')
plt.title('Phase Spectrum'), plt.xticks([]), plt.yticks([])
plt.subplot(2, 2, 4), plt.imshow(reconstruction_magnitude, cmap='gray')
plt.title('Reconstruction'), plt.xticks([]), plt.yticks([])
plt.show()
